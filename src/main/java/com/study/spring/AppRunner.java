package com.study.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import java.util.Arrays;
import java.util.Locale;

@Component
public class AppRunner implements ApplicationRunner {

    @Autowired
    MessageSource messageSource; // 다국어 메세지 기능 제공(ApplicationContext가보면 상속받고 있음)

    @Autowired
    ApplicationEventPublisher publisher;

    @Autowired
    ResourceLoader resourceLoader; // 리소스를 읽어오는 기능을 제공하는 인터페이스

    @Autowired
    Validator validator; // 애플리케이션에서 사용하는 객체 검증용 인터페이스

    // SpEL(Spring Expression Language)
    @Value("#{1 + 1}")
    int value;

    @Value("#{'hello ' + 'world'}")
    String greeting;

    @Value("#{1 eq 1}")
    boolean trueOrFalse;

    // 스프링AOP:프록시 기반 AOP
    @Autowired
    EventService eventService;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        System.out.println("AppRunner");

        // 다국어 적용
        Locale.setDefault(Locale.ROOT);
        System.out.println(messageSource.getMessage("greeting",new String[]{"Hong Jae1"}, Locale.KOREA)); // 스프링부트에서 messages.properties파일만들면 별다른 설정없이 사용가능
        System.out.println(messageSource.getMessage("greeting",new String[]{"Hong Jae2"}, Locale.getDefault()));

        // Event Publisher
        publisher.publishEvent(new MyEvent(this, 100));

        // Resource Loader
        Resource resource = resourceLoader.getResource("classpath:test.txt");
        System.out.println("resource.e   xists : "+resource.exists());

        // Validator
        System.out.println("validator:"+validator.getClass());

        Event event = new Event();
        event.setLimit(-1); // validator test
        event.setEmail("email"); // validator test

        Errors errors = new BeanPropertyBindingResult(event, "event");

        validator.validate(event, errors);
        System.out.println(errors.hasErrors());

        errors.getAllErrors().forEach(e -> {
            System.out.println("========== error code ==========");
            Arrays.stream(e.getCodes()).forEach(System.out::println);
            System.out.println(e.getDefaultMessage());
        });

        // SpEL(Spring Expression Language)
        System.out.println("========== SpEL START ==========");
        System.out.println(value);
        System.out.println(greeting);
        System.out.println(trueOrFalse);
        System.out.println("========== SpEL END ==========");

        eventService.createEvent();
        eventService.publishEvent();
    }
}
