package com.study.spring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
@Aspect
public class PerfAspect {

    @Around("@annotation(PerfLoogging)")
    public Object logPerf(ProceedingJoinPoint pip) throws Throwable {
        long begin = System.currentTimeMillis();
        Object retVal = pip.proceed();
        System.out.println(System.currentTimeMillis() - begin);
        return retVal;
    }

    // AOP ADVICE
    @Before("bean(simpleEventService)")
    public void hello() {
        System.out.println("hello");
    }
}
