package com.study.spring.Config;

import com.study.spring.EventConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        /* Converter등록
        ex) /api/1호출 시, 1이 string->Event타입으로 변환되서 서버에서 처리가능
         */
        registry.addConverter(new EventConverter.StringToEventConverter()); 
    }
}
