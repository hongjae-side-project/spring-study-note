package com.study.spring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventController {

    @GetMapping("/event/{event}")
    public String getEvent(@PathVariable Event event) { // {event}에 있는 값(event id로 정의)을 event타입으로 변환해서 받음
        System.out.println(event);
        return event.getId().toString();
    }
}
