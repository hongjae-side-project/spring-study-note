package com.study.spring;

import java.beans.PropertyEditorSupport;

/*
여러 쓰레드에 공유해서 쓰면안됨(빈으로 등록x)
빈으로 등록하면 1번회원이 2번 회원 변경하는 일들이 발생됨.
*/
public class EventEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        Event event = (Event)getValue();
        return event.getId().toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Event event = new Event();
        event.setId(Integer.parseInt(text));
        setValue(event);
    }
}
