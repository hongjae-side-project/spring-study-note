package com.study.spring;

public interface EventService {
    void createEvent();
    void publishEvent();
}
